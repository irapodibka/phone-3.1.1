public class Main {

    public static void main(String[] args) {

        Nokia nokiaPhone = new Nokia("Nokia", "1111", 11,1, "Red");
        Samsung samsungPhone = new Samsung("Samsung", "2222",
                22, 2, 32, "Moondast black");


        nokiaPhone.call("380671111111");
        samsungPhone.call("380671111111");

        samsungPhone.takePhoto();
        //nokiaPhone.takePhoto(); error because Nokia not implement PhoneMedia interface

        samsungPhone.sendMessage("380671111111","Hello!)");
        nokiaPhone.sendMessage("380671111111","Hello!)");
    }

}
