public interface PhoneConnection {
    void call(String phoneNumber);
    void sendMessage(String phoneNumber,String textMessage);
}
