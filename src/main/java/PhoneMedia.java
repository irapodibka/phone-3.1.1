public interface PhoneMedia {
    void takePhoto();
    void recordVideo();

}
